#![allow(unused_imports)] // TODO: remove
#![recursion_limit="256"]
#![feature(specialization)] // Specialization when???
#![feature(negative_impls)]

// use num_traits::PrimInt;
use core::fmt::Debug;
// use paste::paste;
use std::cmp::{Ord, Ordering};
use rug::ops::{DivRounding, RemRounding, SubFrom};

use replace_with::{replace_with_and_return};

mod errors;
#[allow(unused_imports)]
use errors::{RntError, RntResult};

mod generic;
use generic::Generic;

mod impl_macro;

mod integer_ext;
pub use integer_ext::{ZZ, IntegerExt};

mod gcd_default;
pub use gcd_default::*;

/// General parent trait for any kind of algebraic structure.
///
/// That is, a meta-type (which might also be called the domain or the context)
/// which has a number of sub-traits for all the operations it can perform on
/// its elements (which have type `Self::Element`).
///
/// The term algebraic structure is used a bit loosely here, because the
/// operations are not required to map back to `Self::Element`, but can return
/// anything.
///
/// For example the ring of integers `ZRing` has as its elements instances of
/// `rug::Integer` and includes operations such as addition, primality testing,
/// factoring, etc.
pub trait Algebra: Clone + PartialEq {
  type Element: Clone + PartialEq + Debug;
}

pub trait ContainerAlgebra: Algebra {
  type BaseElement: Clone + PartialEq + Debug;
}

pub trait Rep {
  type RepType;
  fn rep(&self) -> &Self::RepType;
}

macro_rules! defer_to_mut {
  ($slf:ident $meth:ident $owned:ident $borrow:ident) => { {
    let mut r = $owned;
    $slf.$meth(&mut r, $borrow)?;
    Some(r)
  } };
  ($owned:ident as $name:ident { $($code:tt)* }) => { {
    let mut $name = $owned;
    $($code)*
    Some($name)
  } };
}


/// Additive monoid.
///
/// Note that the default implementations assume the monoid is commutative!
pub trait AdditiveMonoid: Algebra {
  alg_fn! {
    fn zero(&self) -> #;
    fn is_zero(&self, a:&#) -> bool;

    fn add_mut(&self, a:&mut #, b:&#)?;

    #[inline]
    fn add_mut_rhs(&self, a:&mut #, b:&#)? {
      self.add_mut(a, b)
    }

    #[inline]
    fn add(&self, a:#, b:&#) -> ?# {
      defer_to_mut!(self add_mut a b)
    }

    #[inline]
    fn add_rhs(&self, a:#, b:&#) -> ?# {
      self.add(a, b)
    }

    fn double_mut(&self,  a:&mut #)?;

    #[inline]
    fn double(&self, a:#) -> ?# {
      defer_to_mut!(a as r { self.double_mut(&mut r)?; })
    }
  }
}

/// Times by repeated addition.
///
/// A default implementation is given for additive groups.
pub trait MonoidTimes: AdditiveMonoid {
  alg_fn! {
    fn times(&self, a:#, b: ZZ) -> ?#;
  }
}

/// Additive groups get a default implementation of times.
impl<T: AdditiveGroup> MonoidTimes for T {
  default fn times(&self, a: Self::Element, b: ZZ) -> Option<Self::Element> {
    Generic::times_zz(self, a, b)
  }
}

/// Additive group: a monoid with negation and subtraction.
///
/// Note that the default implementations assume `a - b => a + -b`.
pub trait AdditiveGroup: AdditiveMonoid {
  alg_fn! {
    fn neg_mut(&self, a:&mut #)?;
    fn sub_mut(&self, a:&mut #, b: &#)?;

    #[inline]
    fn neg(&self, a:#) -> ?# {
      defer_to_mut!(a as r { self.neg_mut(&mut r)?; })
    }

    #[inline]
    fn sub_mut_rhs(&self, a:&mut #, b: &#)? {
      self.neg_mut(a)?;
      self.add_mut(a, b)?;
      Some(())
    }

    #[inline]
    fn sub(&self, a:#, b:&#) -> ?# {
      defer_to_mut!(self sub_mut a b)
    }

    #[inline]
    fn sub_rhs(&self, a:#, b:&#) -> ?# {
      self.add_rhs(self.neg(a)?, b)
    }
  }
}

/// Multiplicative monoid.
///
/// Note that the default implementations assume the monoid is commutative!
pub trait MultiplicativeMonoid: Algebra {
  alg_fn! {
    fn one(&self) -> #;
    fn is_one(&self, a:&#) -> bool;

    fn mul_mut(&self, a:&mut #, b:&#)?;

    #[inline]
    fn mul_mut_rhs(&self, a:&mut #, b:&#)? {
      self.mul_mut(a, b)
    }

    #[inline]
    fn mul(&self, a:#, b:&#) -> ?# {
      defer_to_mut!(self mul_mut a b)
    }

    #[inline]
    fn mul_rhs(&self, a:#, b:&#) -> ?# {
      self.mul(a, b)
    }

    #[inline]
    fn square_mut(&self, a:&mut #)? {
      self.mul_mut(a, &a.clone())?;
      Some(())
    }

    #[inline]
    fn square(&self, a:#) -> ?# {
      defer_to_mut!(a as r { self.square_mut(&mut r)?; })
    }
  }
}

/// Multiplicative group.
///
/// Note that the default implementations assume `x/y = x*inv(y)`.
pub trait MultiplicativeGroup: MultiplicativeMonoid {
  alg_fn! {
    fn inv_mut(&self, a:&mut #)?;

    fn div_mut(&self, a:&mut #, b:&#)?;
    fn div_rhs_mut(&self, a:&mut #, b:&#)?;

    #[inline]
    fn inv(&self, a:#) -> ?# {
      defer_to_mut!(a as r { self.inv_mut(&mut r)?; })
    }

    #[inline]
    fn div(&self, a:#, b:&#) -> ?# {
      defer_to_mut!(self div_mut a b)
    }

    #[inline]
    fn div_rhs(&self, a:#, b:&#) -> ?# {
      defer_to_mut!(self div_rhs_mut a b)
    }
  }
}

/// Power by repeated multiplication.
///
/// A default implementation is given for multiplicative groups.
pub trait MonoidPower: MultiplicativeMonoid {
  alg_fn! {
    fn power(&self, a:#, b: ZZ) -> ?#;
  }
}

/// Default implementation of `A.power(a,n)` for multiplicative groups.
impl<T: MultiplicativeGroup> MonoidPower for T {
  default fn power(&self, a: Self::Element, b: ZZ) -> Option<Self::Element> {
    Generic::power_zz(self, a, b)
  }
}


pub trait Ring: AdditiveGroup + MultiplicativeMonoid {
  alg_fn! {
    /// Maps an integer into the ring.
    ///
    /// Should be equivalent to:
    /// 1 + 1 + ... + 1 + 1
    /// \------- n -------/
    fn from_integer(&self, a: ZZ) -> #;

    /// Multiply-and-add.
    ///
    /// `a += b * c`
    #[inline]
    fn muladd_mut(&self, a:&mut #, b:&#, c:&#)? {
      let r = self.mul(b.clone(), c)?;
      self.add_mut(a, &r)
    }

    #[inline]
    fn mulsub_mut(&self, a:&mut #, b:&#, c:&#)? {
      let r = self.mul(b.clone(), c)?;
      self.sub_mut(a, &r)
    }
  }
}

/// GCD domains.
///
/// This is not strict, these are more like pseudo-Euclidean domains.
pub trait GCDDomain: Ring {
  alg_fn! {
    fn gcd(&self, a:#, b:#) -> ?#;
    fn gcdext(&self, a:#, b:#) -> ?(#,#,#);
    fn lcm(&self, a:#, b:#) -> ?#;
  }
}

macro_rules! defer_to_owned {
  ($self:ident, $val:ident; $t:ident => $($x:tt)*) => {
    replace_with_and_return($val, || $self.zero(), |$t| {
      match $($x)* {
        Some(v) => (Some(()), v),
        None => (None, $self.zero()),
      }
    })
  };

  ($self:ident, $u:ident, $v:ident; $t:ident,$r:ident => $($x:tt)*) => {
    replace_with_and_return($u, || $self.zero(), |$t| {
      replace_with_and_return($v, || $self.zero(), |$r| match $($x)* {
        Some((x,y)) => ((Some(()), x), y),
        None => ((None, $self.zero()), $self.zero()),
      })
    })
  }
}

// Option<T> => (Option<()>, )
pub trait EuclideanDomain: Ring {
  alg_fn! {
    #[inline]
    fn div_mut(&self, a:&mut #, b:&#)? {
      defer_to_owned!(self, a; t => self.div(t, b))
    }

    #[inline]
    fn div_rhs_mut(&self, a:&mut #, b:&#)? {
      defer_to_owned!(self, a; t => self.div_rhs(t, b))
    }

    #[inline]
    fn rem_mut(&self, a:&mut #, b:&#)? {
      defer_to_owned!(self, a; t => self.rem(t, b))
    }
    #[inline]
    fn rem_rhs_mut(&self, a:&mut #, b:&#)? {
      defer_to_owned!(self, a; t => self.rem_rhs(t, b))
    }

    #[inline]
    fn divrem_mut(&self, a:&mut #, b:&mut #)? {
      defer_to_owned!(self, a, b; u, v => self.divrem(u, v))
    }
    #[inline]
    fn divrem_rhs_mut(&self, a:&mut #, b:&mut #)? {
      defer_to_owned!(self, a, b; u, v => self.divrem_rhs(u, v))
    }

    #[inline]
    fn div(&self, a:#, b:&#) -> ?# {
      defer_to_mut!(self div_mut a b)
    }
    #[inline]
    fn div_rhs(&self, a:#, b:&#) -> ?# {
      defer_to_mut!(self div_rhs_mut a b)
    }
    #[inline]
    fn rem(&self, a:#, b:&#) -> ?# {
      defer_to_mut!(self rem_mut a b)
    }
    #[inline]
    fn rem_rhs(&self, a:#, b:&#) -> ?# {
      defer_to_mut!(self rem_rhs_mut a b)
    }

    #[inline]
    fn divrem(&self, a:#, b:#) -> ?(#,#) {
      let (mut ra, mut rb) = (a, b);
      self.divrem_mut(&mut ra, &mut rb)?;
      Some((ra, rb))
    }
    #[inline]
    fn divrem_rhs(&self, a:#, b:#) -> ?(#,#) {
      self.divrem(b, a)
    }
  }
}

/// Fields.
///
pub trait Field: Ring + MultiplicativeGroup {
  // Multiplicative group has inv.

  // alg_fn! {
  //   #[inline]
  //   fn inv_mut(&self, a:&mut #)? {
  //     replace_mut(a, |a| self.inv(a))
  //   }

  //   #[inline]
  //   fn inv(&self, a:#) -> ?# {
  //     defer_to_mut!(a as r { self.inv_mut(&mut r)?; })
  //   }
  // }
}

/// Shiftable.
///
pub trait Shiftable: Algebra {
  alg_fn! {
    fn shl_mut(&self, a:&mut #, n: ZZ)?;
    fn shr_mut(&self, a:&mut #, n: ZZ)?;
  }
}

pub trait VectorRep: Algebra {
  type RepType;
  fn vecrep(&self, el: &Self::Element) -> &[Self::RepType];
}

pub trait InnerProductSpace: ContainerAlgebra {
  fn inner_product(&self, a: &Self::Element, b: &Self::Element) -> Option<Self::BaseElement>;
}


mod z_ring;
pub use z_ring::ZRing;

mod z_mod;
pub use z_mod::{ZMod, ZModElem};

mod polynomial;
pub use polynomial::{Polynomial, PolyElem};

mod cartesian;
pub use cartesian::{Cartesian};

mod static_vector;
mod dyn_vector;
mod dyn_matrix;
pub use dyn_matrix::{MatrixSpace, MatElem};
