use array2d::Array2D;
use super::*;

/// # Generic matrices of non-static size.
///
#[derive(Clone,PartialEq)]
pub struct MatrixSpace<A>(A,usize,usize);

pub type MatElem<E> = Array2D<E>;

impl<A: Algebra> Algebra for MatrixSpace<A> {
  type Element = MatElem<A::Element>;
}

impl<A: Algebra> ContainerAlgebra for MatrixSpace<A> {
  type BaseElement = A::Element;
}

impl<A: Algebra> MatrixSpace<A> {
  pub fn new(domain: impl Into<A>, rows: usize, cols: usize) -> Self {
    MatrixSpace(domain.into(), rows, cols)
  }

  pub fn elem(&self, els: Array2D<A::Element>) -> MatElem<A::Element> {
    els
  }

  pub fn elem_from_vecs(&self, els: &Vec<Vec<A::Element>>) -> Option<MatElem<A::Element>> {
    if els.len() != self.1 {
      return None
    }
    if els.iter().any(|x| x.len() != self.2) {
      return None
    }
    let arr = Array2D::from_rows(els);
    Some(self.elem(arr))
  }

  pub fn new_diagonal(&self, d: &A::Element, z: &A::Element) -> MatElem<A::Element> {
    Array2D::from_iter_row_major(
      (0..self.1).flat_map(|r| (0..self.2).map(move |c| if r == c { d.clone() } else { z.clone() })),
      self.1, self.2)
  }
}

impl<A: Ring> Ring for MatrixSpace<A> {
  fn from_integer(&self, a: ZZ) -> Self::Element {
    if a.cmp0().is_eq() {
      self.zero()
    } else if &a == &(1 as u32) {
      self.one()
    } else {
      self.new_diagonal(&self.0.from_integer(a), &self.0.zero())
    }
  }
}

impl<A: AdditiveMonoid> AdditiveMonoid for MatrixSpace<A> {
  fn zero(&self) -> Self::Element {
    Array2D::filled_with(self.0.zero(), self.1, self.2)
  }

  fn is_zero(&self, a: &Self::Element) -> bool {
    for u in a.elements_row_major_iter() {
      if !self.0.is_zero(u) {
        return false;
      }
    }
    true
  }

  fn add_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    if a.num_rows() != b.num_rows() { return None; }
    if a.num_columns() != b.num_columns() { return None; }
    for r in 0..a.num_rows() {
      for c in 0..a.num_columns() {
        self.0.add_mut(&mut a[(r,c)], &b[(r,c)])?;
      }
    }
    Some(())
  }

  fn double_mut(&self, a: &mut Self::Element) -> Option<()> {
    for r in 0..a.num_rows() {
      for c in 0..a.num_columns() {
        self.0.double_mut(&mut a[(r,c)])?;
      }
    }
    Some(())
  }
}

impl<A: AdditiveGroup> AdditiveGroup for MatrixSpace<A> {
  fn neg_mut(&self, a: &mut Self::Element) -> Option<()> {
    for r in 0..a.num_rows() {
      for c in 0..a.num_columns() {
        self.0.neg_mut(&mut a[(r,c)])?;
      }
    }
    Some(())
  }

  fn sub_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    if a.num_rows() != b.num_rows() { return None; }
    if a.num_columns() != b.num_columns() { return None; }
    for r in 0..a.num_rows() {
      for c in 0..a.num_columns() {
        self.0.sub_mut(&mut a[(r,c)], &b[(r,c)])?;
      }
    }
    Some(())
  }
}

impl<A: Ring> MultiplicativeMonoid for MatrixSpace<A> {
  fn one(&self) -> Self::Element {
    let mut arr = self.zero();
    for i in 0..arr.num_rows().min(arr.num_columns()) {
      arr[(i,i)] = self.0.one();
    }
    arr
  }

  fn is_one(&self, el: &Self::Element) -> bool {
    for i in 0..el.num_rows() {
      for (j,el) in el.row_iter(i).enumerate() {
        if i == j && !self.0.is_one(el) {
          return false;
        }
        if i != j && !self.0.is_zero(el) {
          return false;
        }
      }
    }
    true
  }

  fn mul_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    if a.num_columns() != b.num_rows() {
      return None
    }
    let (rows, cols) = (a.num_rows(), b.num_columns());

    let mut tmp = Vec::with_capacity(rows * cols);
    for r in 0..rows {
      for c in 0..cols {
        let mut s = self.0.zero();
        for (av,bv) in a.row_iter(r).zip(b.column_iter(c)) {
          self.0.muladd_mut(&mut s, &av, &bv)?;
        }
        tmp.push(s)
      }
    }
    *a = Array2D::from_iter_row_major(tmp.into_iter(), rows, cols);
    Some(())
  }
}
