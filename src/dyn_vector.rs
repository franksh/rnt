use super::*;

/// # Vectors w/ fixed (but not compile-time) sizes.
///
#[derive(Clone,PartialEq)]
pub struct DynVector<A>(A,usize);

impl<A: Algebra> Algebra for DynVector<A> {
  type Element = Vec<A::Element>;
}

impl<A: Algebra> ContainerAlgebra for DynVector<A> {
  type BaseElement = A::Element;
}

impl<A: AdditiveMonoid> AdditiveMonoid for DynVector<A> {
  fn zero(&self) -> Self::Element {
    std::iter::repeat(self.0.zero()).take(self.1).collect()
  }

  fn is_zero(&self, a: &Self::Element) -> bool {
    for u in a {
      if !self.0.is_zero(u) {
        return false;
      }
    }
    true
  }

  fn add_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    if a.len() != b.len() { return None; }
    for (i,v) in b.iter().enumerate() {
      self.0.add_mut(&mut a[i], v)?;
    }
    Some(())
  }

  fn double_mut(&self, a: &mut Self::Element) -> Option<()> {
    for v in a.iter_mut() {
      self.0.double_mut(v)?;
    }
    Some(())
  }
}

impl<A: AdditiveGroup> AdditiveGroup for DynVector<A> {
  fn neg_mut(&self, a: &mut Self::Element) -> Option<()> {
    for v in a.iter_mut() {
      self.0.neg_mut(v)?;
    }
    Some(())
  }

  fn sub_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    if a.len() != b.len() { return None; }
    for (i,v) in b.iter().enumerate() {
      self.0.add_mut(&mut a[i], v)?;
    }
    Some(())
  }
}

impl<A: Ring> InnerProductSpace for DynVector<A> {
  fn inner_product(&self, a: &Self::Element, b: &Self::Element) -> Option<Self::BaseElement> {
    if a.len() != b.len() { return None; }
    let mut s = self.0.zero();
    for (a,b) in a.iter().zip(b.iter()) {
      self.0.muladd_mut(&mut s, &a, &b)?;
    }
    Some(s)
  }
}
