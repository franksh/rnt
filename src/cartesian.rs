use super::*;

/// # Cartesian power of an algebra.
///
/// Which is a fancy way of saying vectors of elements over said algebra using
/// element-wise operations.
///
/// The elements are of type Rust vectors `Vec<R>`. The only caveat is that the
/// lenghts need to match.
#[derive(Clone,PartialEq)]
pub struct Cartesian<A>(A, usize);

impl<A: Algebra> Algebra for Cartesian<A> {
  type Element = Vec<A::Element>;
}

impl<A: Algebra> Cartesian<A> {
  pub fn new(domain: impl Into<A>, size: impl Into<usize>) -> Self {
    Cartesian(domain.into(), size.into())
  }

  #[inline]
  pub fn elem(&self, els: Vec<A::Element>) -> Vec<A::Element> {
    els
  }
}

// impl<A: AdditiveMonoid> AlgebraFrom<Vec<A::Element>> for Cartesian<A> {
//   default fn from<V: Into<Vec<A::Element>>>(&self, els: V) -> Self::Element {
//     let vs = els.into();
//     assert_eq!(self.1, vs.len());
//     vs
//   }
// }

// impl<A: AdditiveMonoid + AlgebraFromInteger> AlgebraFromInteger for Cartesian<A> {
//   fn from_integer(&self, el: ZZ) -> Self::Element {
//     std::iter::repeat(self.0.from_integer(el)).take(self.1).collect()
//   }
// }

impl<A: AdditiveMonoid> AdditiveMonoid for Cartesian<A> {
  fn zero(&self) -> Self::Element {
    std::iter::repeat(self.0.zero()).take(self.1).collect()
  }
  fn is_zero(&self, a: &Self::Element) -> bool {
    for u in a {
      if !self.0.is_zero(u) {
        return false;
      }
    }
    true
  }
  fn add_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    if a.len() != b.len() { return None; }
    for (i,v) in b.iter().enumerate() {
      self.0.add_mut(&mut a[i], v)?;
    }
    Some(())
  }
  fn double_mut(&self, a: &mut Self::Element) -> Option<()> {
    for v in a.iter_mut() {
      self.0.double_mut(v)?;
    }
    Some(())
  }
}

// impl<A: AdditiveGroup> AdditiveGroup for Cartesian<A> {

// }
