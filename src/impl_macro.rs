
#[allow(unused_imports)]
use super::{Algebra, RntResult};

/// TODO: remove this very personal and ignorant rant.
///
/// Let's go on an adventure, shall we?
///
/// Your task is the following: use Rust macros to write a DSL for simplified
/// and repetitive trait definitions, in that it replaces every instance of the
/// token "#" (arbitrary, could be anything) with the tokens "Self::Element".
/// There should also be some mechanism for easily specifying that the return
/// type should be wrapped in "RntResult<..., Self::Element>".
///
/// "Why? Just to save a few keystrokes?"
///
/// Well, yeah. It's not about the keystrokes, though. I have wasted many more
/// keystrokes on solving this than it would have cost me to just type it out or
/// copy-paste it. Let's say your motivation is sheer defiance. Because you
/// believe this _ought to be easy_. After all, it's just a simple replacement
/// of some token, with some added conveniences!
///
/// "Why not use a type alias or something?"
///
/// Again, it's not about that. I hate being a user of Rust and every time I or
/// anyone asks a question about Rust the answers tend to come in two forms:
///
/// a) "that's not yet possible, but it's planned!" Which usually comes with a
/// link to a 6 year old GitHub issue with little or no progress. This is just
/// pure Stockholm syndrome. (c.f. GATs, specialization, const generics, blah
/// blah.)
///
/// b) "instead of doing that, you should probably do <foo> instead." This is
/// often well-intentioned and sometimes give very good suggestions. But
/// sometimes it doesn't feel very good, when I (the user) believe what I'm
/// trying to do is actually quite reasonable.
///
/// "Use procedural macros."
///
/// Yeah... Fair enough. But they need to be in their own crate and that is
/// hugely inconvenient in my opinion. They're also 20x more complicated to use.
/// I'd like to avoid them if possible, and I know it's possible. Again, I feel
/// what I want to achieve isn't that crazy.
///
/// Besides, the situation we're in is entirely arbitrary. The point is to
/// explore Rust macros for something other than the typical examples given of
/// how awesome Rust macros are. The well-behaved utility macros that don't try
/// to interact with generics or traits.
///
/// As of right now, even with nightly, in the year 2021, Rust macros have some
/// problems:
///
/// A.1. No backtracking. This is a big pain point. Try matching on the last
/// item of a list of tokens. As far as I know the only way is to first reverse
/// the list, then match on that.
///
/// A.2. (related) "Optional" parsing and using "defaults" suffer a
/// combinatorial explosion in the arguments.
///
/// https://users.rust-lang.org/t/macros-avoiding-an-explosion-of-patterns-for-optional-parts-cleanly/8124
///
/// A.3. (related) Also: no lookahead.
///
/// A.4. ... all of which forces users to implement an unreadable and ugly ad
/// hoc top-down regular parser (i.e. a "tt-muncher") to do stupidly simple
/// stuff.
///
/// B.1. The fragment specifiers (meta variable types) feel like a hack. If the
/// syntax level you care about matches one of the specifiers exactly, then
/// great. If not...well, time to implement your own parser. Over time the list
/// of specifiers has also grown, and there's new requests all the time. Either
/// just expose all or most AST node types, or find an alternative.
///
/// B.2. Macros are only invoked at the tokenization/parsing level, so no
/// semantic or type information is available. Macro writers must awkwardly ask
/// users to add an extra argument to specify the type of an expression if you
/// happen to need it in the macro.
///
/// Let's compare with another compiled systems language: in Nim, you can
/// choose, for each argument, whether the parser should delay invoking the
/// macro until after that argument has been semantically parsed, until it has
/// been fully type checked, or if it should just be given as a token tree
/// without attempting to parse it first.
///
/// This is kind of like Rust's fragment specifiers but a lot simpler and much
/// better represents what many users actually want. It is, in my opinion, a
/// more elegant solution, where Rust's macros become a special case (if you
/// specify you only want untyped arguments).
///
/// B.3. The level Rust macros operate on is a bit of a mystery... They _don't_
/// operate on token streams, since you can't match unbalanced delimiters (e.g.
/// a single `<`), nor do they operate on an actual tree because you can't go up
/// or down. They functionally operate on some kind of serialization process of
/// a tree, a process which you can't control.
///
/// C.1. There's no way(?)
/// to give the output of one macro to another macro. I'm not even sure this is
/// possible with procedural macros, because whenever I've used macros inside
/// external `#[magic]` derive or attribute items, the enclosing macro has never
/// been able to handle it.
///
/// C.2. To pass stuff to another macro you have to use a counterintuitive and
/// awkward continuation passing style.
///
/// C.3. ...which can lead to quickly hitting the recursion limit, because
/// macros don't have tail-call optimization or anything like that, as they were
/// never designed to be a language.
///
/// D. Extreme hygiene can become a pathology. Never mind the general ability to
/// do injections for the sake of convenience. Just focus on the most painful
/// one which is `self`. `self` is clearly special and _almost_ a keyword (after
/// all, you can use `Self`!), yet you have to sheepishly ask users to fill in a
/// dummy `$slf:ident` with the self-identifier so you can actually use it.
///
/// E. `concat_idents!()`, even though being a nightly feature, is laughably
/// pathetic and weak. It's a damn mark of shame that for something so simple
/// and fundamental you have to defer to an external crate like `paste`.
///
/// F. And yes, the split between proc-macros and regular macros, and how
/// inconvenient proc-macros are to use.
///
/// Nim also has two macro constructions, the fully powered `macro` which is
/// sort of like proc-macros in Rust, but you don't have to fork it off into its
/// own crate. You get the full syntax tree, compile-time reflection, what have
/// you, and can write regular Nim code to manipulate it.
///
/// Of course, most of the time it's unnecessary, as the much, much simpler and
/// elegant alternative `template` (having none of the incredible verbosity of
/// regular Rust macros) usually works for adding language features and
/// constructs.
///
/// Anyway, so back to the problem at hand. You want to be able to write
/// something like this:
///
/// ```ignore
/// some_macro! {
///   add_mut(&self, x: &mut #, y: &#)?;
///   add(&self, x: #, y: &#) -> ?# {
///     let mut r = x;
///     self.add_mut(&mut r, y)?;
///     Ok(r)
///   }
///   zero(&self) -> #;
///   is_one(&self, x: &#) -> bool;
///   divmod(&self, x: #, y: #) -> (#,#)?;
/// }
/// ```
///
/// And it should turn into this:
///
/// ```ignore
/// fn add_mut(&self, x: &mut Self::Element, y: &Self::Element) -> RntResult<(), Self::Element>;
/// // etc...
/// ```
///
/// Looks super easy!
///
/// Or is it...?
///
/// Below is my attempt... Originally my plan was to also support shortening
/// "x:&#" and similar into "&x" but I gave up on that.
///
/// I now _hate_ Rust macros.
///
/// Edit: conditions have changed, code has been modified since the above rant.


// The macro that actually does the replacement `# -> Self::Element` on a series
// of tokens. It is called with an argument of the macro to call back with the
// result, since macros can't "return" anything. Note that it also recurses into
// parenthetical groups.
#[macro_export]
macro_rules! mac_replace_hash {
  // We're done parsing.
  ([] [$($args:tt)*] $cb:ident $($tail:tt)*) => {
    $cb! { @A[$($args)*] $($tail)* }
  };

  // Recurse into () and <>.
  ([($($x:tt)*) $($xs:tt)*] [$($args:tt)*] $cb:ident $($tail:tt)*) => {
    mac_replace_hash! { [$($x)*] [] mac_replace_hash () [$($xs)*] [$($args)*] $cb $($tail)* }
  };
  ([<$($x:tt)*> $($xs:tt)*] [$($args:tt)*] $cb:ident $($tail:tt)*) => {
    mac_replace_hash! { [$($x)*] [] mac_replace_hash <> [$($xs)*] [$($args)*] $cb $($tail)* }
  };

  // Resume from a callback.
  (@A[$($res:tt)*] () [$($xs:tt)*] [$($args:tt)*] $cb:ident $($tail:tt)*) => {
    mac_replace_hash! { [$($xs)*] [$($args)* ( $($res)* )] $cb $($tail)* }
  };
  (@A[$($res:tt)*] <> [$($xs:tt)*] [$($args:tt)*] $cb:ident $($tail:tt)*) => {
    mac_replace_hash! { [$($xs)*] [$($args)* < $($res)* >] $cb $($tail)* }
  };

  // The actual replacement. ALL THIS BULLSHIT FOR THIS.
  ([# $($xs:tt)*] [$($args:tt)*] $cb:ident $($tail:tt)*) => {
    mac_replace_hash! { [$($xs)*] [$($args)* Self :: Element] $cb $($tail)* }
  };

  // No matches, just shift.
  ([$x:tt $($xs:tt)*] [$($args:tt)*] $cb:ident $($tail:tt)*) => {
    mac_replace_hash! { [$($xs)*] [$($args)* $x] $cb $($tail)* }
  };
}

// The inner macro that actually writes out the fn implementation just
// exponentially explodes over its arguments.
//
// G. (problems with macros, cont.) I can't find a way to force a meta attribute
// to apply to the function that a macro call will expand to instead of applying
// to the macro invocation itself? E.g. this doesn't work:
//
// ```ignore
// (@A[$($stuff:tt)*] @M[$($met:meta),*] $name:ident $($tail:tt)*) => {
//   $( #[ $met ] )*
//     alg_fn_impl! { @A[$($stuff)*] $($tail)* $name }
// };
// ```
#[macro_export]
macro_rules! alg_fn_impl {
  (@A[?]@M[$($met:meta)*][$($args:tt)*][] $name:ident) => {
    $( #[ $met ] )*
    fn $name($($args)*) -> Option<()> ;
  };
  (@A[?]@M[$($met:meta)*][$($args:tt)*][$($body:tt)+] $name:ident) => {
    $( #[ $met ] )*
    fn $name($($args)*) -> Option<()> { $($body)* }
  };
  (@A[? $($ret:tt)+]@M[$($met:meta)*][$($args:tt)*][] $name:ident) => {
    $( #[ $met ] )*
    fn $name($($args)*) -> Option<$($ret)*> ;
  };
  (@A[? $($ret:tt)+]@M[$($met:meta)*][$($args:tt)*][$($body:tt)+] $name:ident) => {
    $( #[ $met ] )*
    fn $name($($args)*) -> Option<$($ret)*> { $($body)* }
  };
  (@A[]@M[$($met:meta)*][$($args:tt)*][] $name:ident) => {
    $( #[ $met ] )*
    fn $name($($args)*);
  };
  (@A[]@M[$($met:meta)*][$($args:tt)*][$($body:tt)+] $name:ident) => {
    $( #[ $met ] )*
    fn $name($($args)*) { $($body)* }
  };
  (@A[$($ret:tt)+]@M[$($met:meta)*][$($args:tt)*][] $name:ident) => {
    $( #[ $met ] )*
    fn $name($($args)*) -> $($ret)*;
  };
  (@A[$($ret:tt)+]@M[$($met:meta)*][$($args:tt)*][$($body:tt)+] $name:ident) => {
    $( #[ $met ] )*
    fn $name($($args)*) -> $($ret)* { $($body)* }
  };
}

#[macro_export]
macro_rules! alg_fn {
  // We're done.
  () => { };

  // Collect meta attributes.
  (@M[$($mets:meta)*] #[$m:meta] $($tail:tt)*) => {
    alg_fn! { @M[$($mets)* $m] $($tail)* }
  };

  // Parse the "func(&self, x,y)" part first and call back to this macro with
  // the result.
  (@M[$($met:meta)*] fn $name:ident ($($args:tt)*) $($tail:tt)*) => {
    mac_replace_hash! { [$($args)*] [] alg_fn $name @M[$($met)*] [] $($tail)* }
  };

  // Function is done -- body was missing.
  (@A[$($res:tt)*] $name:ident @M[$($met:tt)*] [$($ret:tt)*] ; $($tail:tt)*) => {
    mac_replace_hash! { [$($ret)*] [] alg_fn_impl @M[$($met)*] [$($res)*] [] $name }
    alg_fn! { $($tail)* }
  };
  // Function is done -- with body specification.
  (@A[$($res:tt)*] $name:ident @M[$($met:tt)*] [$($ret:tt)*] { $($body:tt)* } $($tail:tt)*) => {
    mac_replace_hash! { [$($ret)*] [] alg_fn_impl @M[$($met)*] [$($res)*] [$($body)*] $name }
    alg_fn! { $($tail)* }
  };
  // Simplify macro by simply ignoring ->. This is fucked if -> is in return
  // type, but it can't be...right?
  (@A[$($res:tt)*] $name:ident @M[$($met:tt)*] [$($ret:tt)*] -> $($tail:tt)*) => {
    alg_fn! { @A[$($res)*] $name @M[$($met)*] [$($ret)*] $($tail)* }
  };
  // Add token to list to be fed to mac_replace_hash!().
  (@A[$($res:tt)*] $name:ident @M[$($met:tt)*] [$($ret:tt)*] $x:tt $($tail:tt)*) => {
    alg_fn! { @A[$($res)*] $name @M[$($met)*] [$($ret)* $x] -> $($tail)* }
  };

  // First...unintuitively last.
  (fn $($tail:tt)*) => {
    alg_fn! { @M[] fn $($tail)* }
  };
  (# $($tail:tt)*) => {
    alg_fn! { @M[] # $($tail)* }
  };
}
