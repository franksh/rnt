use super::*;

/// # Generic vectors w/ compile-time static size.
///
#[repr(transparent)]
#[derive(Clone,PartialEq,Debug)]
pub struct Vector<A,const N: usize>(A);

#[repr(transparent)]
#[derive(Clone,PartialEq,Hash,Debug)]
pub struct VecElem<E,const N: usize>([E; N]);

impl<E: Sized + Default, const N: usize> Default for VecElem<E,N> {
  fn default() -> Self {
    let mut arr: [std::mem::MaybeUninit<E>;N] = unsafe { std::mem::MaybeUninit::uninit().assume_init() };
    for item in &mut arr[..] {
      unsafe { std::ptr::write(item.as_mut_ptr(), E::default()); }
    }
    // XXX: https://github.com/rust-lang/rust/issues/62875
    let ret = VecElem(unsafe { std::mem::transmute_copy::<_, [E;N]>(&arr) });
    std::mem::forget(arr);
    ret
  }
}

impl<A: Algebra, const N: usize> Algebra for Vector<A,N> {
  type Element = VecElem<A::Element,N>;
}

impl<A: Algebra, const N: usize> ContainerAlgebra for Vector<A,N> {
  type BaseElement = A::Element;
}
