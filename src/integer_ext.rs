
use std::ops::{Bound, Mul, RangeBounds};

use num_traits::PrimInt;
use rug::{Integer, integer::{Order, SmallInteger}, ops::*};

pub type ZZ = rug::Integer;


/// Extract discrete range from a `RangeBounds<U>`.
fn discrete_bounds<T: PrimInt, U: PrimInt>(range: impl RangeBounds<U>, min: T, max: T) -> (T, T) {
  (match range.start_bound() {
    Bound::Included(v) => T::from(*v).unwrap(),
    Bound::Excluded(v) => T::from(*v+U::one()).unwrap(),
    Bound::Unbounded => min,
  },
   match range.end_bound() {
    Bound::Included(v) => T::from(*v).unwrap(),
    Bound::Excluded(v) => T::from(*v-U::one()).unwrap(),
    Bound::Unbounded => max,
  })
}

pub trait IntegerExt: Sized {
  fn trailing_zeros(&self) -> u32;
  fn make_odd(self) -> (Self, u32);
  fn twos_complement_bytes(&self, little_endian: bool) -> Vec<u8>;
  fn twos_complement_bytes_padded(&self, little_endian: bool, min_width: usize) -> Vec<u8>;
  fn digit_list(&self, base: &Self, width: impl RangeBounds<usize>) -> Vec<Self>;
  fn sqrt_prime_mod(self, p: &Self) -> Option<Self>;
  fn abs_mod(self, p: &Self) -> Self;
  fn odd_nonresidue(&self) -> u32;
  // fn small_factors(self, upto: usize) -> (Self, Vec<(usize, u32)>) where Self: Sized;
  fn root_prime_mod(self, p: &Self);
}

impl IntegerExt for Integer {
  fn trailing_zeros(&self) -> u32 {
    let mut e = 0;
    for &l in self.as_limbs() {
      e += l.trailing_zeros();
      if l != 0 {
        break;
      }
    }
    e
  }

  fn make_odd(self) -> (Self, u32) {
    let e = self.trailing_zeros();
    (self >> e, e)
  }

  fn twos_complement_bytes_padded(&self, little_endian: bool, min_width: usize) -> Vec<u8> {
    let mut q;
    let (tmp, fill) = if self.cmp0().is_ge() {
      (self, 0)
    } else {
      let bit = self.significant_bits();
      q = self.as_abs().clone();
      q.toggle_bit(bit);
      q.keep_signed_bits_mut(bit);
      q.abs_mut();
      for i in bit..bit+8-(bit & 0x7) {
        q.toggle_bit(i);
      }
      (&q, 0xffu8)
    };
    let sz = tmp.significant_digits::<u8>();
    let mut res = vec![fill; sz.max(min_width)];
    if little_endian {
      tmp.write_digits::<u8>(&mut res[..sz], Order::Lsf);
    } else {
      tmp.write_digits::<u8>(&mut res[sz.max(min_width)-sz..], Order::Msf);
    }
    res
  }

  fn twos_complement_bytes(&self, little_endian: bool) -> Vec<u8> {
    self.twos_complement_bytes_padded(little_endian, 0)
  }

  fn digit_list(&self, base: &Self, width: impl RangeBounds<usize>) -> Vec<Self> {
    assert!(base.cmp_abs(&1.into()).is_gt());
    let mut res = Vec::new();
    let (min_w, max_w) = discrete_bounds(width, 1usize, usize::MAX);
    let mut q = self.clone().abs();
    while q.cmp0().is_ne() && res.len() <= max_w {
      let mut r = base.clone();
      q.div_rem_euc_mut(&mut r);
      res.push(r);
    }
    while res.len() < min_w {
      res.push(Integer::default());
    }
    res
  }

  /// Compute a square root under modulus `p`.
  ///
  /// Assumes `p` is prime!
  ///
  /// Code adapted from earlier Python code in my flagmining library.
  fn sqrt_prime_mod(self, p: &Self) -> Option<Self> {
    let a = if self.cmp0().is_gt() && self.cmp(p).is_lt() {
      self
    } else {
      let a = self.rem_euc(p);
      if a.cmp0().is_eq() {
        return Some(a);
      }
      a
    };

    // Case #1: p is even.
    if p.is_even() {
      assert!(p.significant_bits() == 2);
      return Some(a)
    }

    // Check that we're actually a quadratic residue.
    let is_res = a.jacobi(p); // in GMP, jacobi and legendre is the same function.
    if is_res != 1 {
      return None;
    }

    let m = p.mod_u(8);

    // Case #2: p = 3 (mod 4).
    if m == 3 || m == 7 {
      let n = Self::from(p + 1) >> 2;
      return Some(a.pow_mod(&n, p).unwrap_or_default());
    }

    // Case #3: p = 5 (mod 8).
    if m == 5 {
      // For this case we know 2 is a primitive root so we can speed the
      // calculation up.

      let k = Self::from(p - 5) >> 3;
      // p == 8k + 5
      let mut aq = a.clone();
      let q = a.pow_mod(&k, p).unwrap_or_default();
      aq.mul_from(&q);
      // aq == a^(k+1)

      let r = q.mul(&aq).rem_euc(p);
      // r == a^(2k+1)

      // assert r == 1 or r == p-1
      if r == 1 {
        return Some(aq.rem_euc(p));
      } else {
        let p2 = Integer::from(2).pow_mod(&(k*2 + 1), p).unwrap(); // Cannot fail.
        return Some(aq.mul(p2).rem_euc(p));
      }
    }

    // Now for the harder case of p == 8k+1.

    // Here be dragons! Read the paper "Square roots from 1; 24, 51, 10 to Dan
    // Shanks" by Ezra Brown for more information

    // Partition p-1 to 2^e*q for an odd q.
    let (q,e) = Self::from(p - 1).make_odd();

    // x is a guess of the square root that gets better with each iteration. b
    // is the inverse complement of x^2 to a (aka `self`). The invariant
    // throughout the loop is x^2 = ab (mod p).
    //
    // tmp == a^(q//2).
    let tmp = a.clone().pow_mod(&Self::from(&q >> 1), &p).unwrap();
    // x == a^(q//2+1).
    let mut x = a.mul(&tmp);
    // b == a^(q//2 + q//2 + 1) == a^(q-1+1) == a^q.
    let mut b = tmp.mul(&x);
    // x^2 == ab.

    // We need a non-residue `g` such that g^(2^e) == 1. We use this to update
    // `x` and `b`. We know 2, 4, etc. is always a quadratic residue under p ==
    // 8k+1 so just test odd numbers.
    let mut g = Self::from(p.odd_nonresidue()).pow_mod(&q, p).unwrap();
    let mut r = e;

    loop {
      let mut t = b.clone().rem_euc(p);
      let mut m = 0;
      while t != 1 {
        assert!(m < e);
        t = t.square().rem_euc(p);
        m += 1;
      }
      if m == 0 {
        return Some(x.rem_euc(p));
      }
      assert!(m < r);

      let gs = g.pow_mod(&SmallInteger::from(1 << (r-m-1)), p).unwrap();
      x = x.mul(&gs);
      // Save on some divisions by not always reducing modulo p.
      g = gs.square();
      b = b.mul(&g);
      r = m
    }
  }

  fn abs_mod(mut self, p: &Self) -> Self {
    if self.significant_bits() + 1 >= p.significant_bits() && self.cmp(&Self::from(p >> 1)).is_gt() {
      self.sub_from(p);
    }
    self
  }

  /// First odd non-residue modulo `self`. Assumes `self` is odd.
  fn odd_nonresidue(&self) -> u32 {
    for k in (3..).step_by(2) {
      if Self::from(k).jacobi(self) == -1 {
        return k;
      }
    }
    unreachable!();
  }

  // fn small_factors(self, until: usize) -> (Self, Vec<(usize, u32)>) {
  //   let mut n = self;
  //   let mut fs = Vec::new();
  //   if until <= 2 || n.cmp0().is_le() {
  //     return (n, fs);
  //   }
  //   if n.is_even() {
  //     let (nn, e) = n.make_odd();
  //     fs.push((2, e));
  //     n = nn;
  //   }
  //   for p in primal::Sieve::new(until).primes_from(3) {
  //     let e = n.remove_factor_mut(&SmallInteger::from(p));
  //     if e > 0 {
  //       fs.push((p, e));
  //     }
  //   }
  //   (n, fs)
  // }

  fn root_prime_mod(self, _p: &Self) {
    unimplemented!();
  }
}


/// Generates a random _odd_ prime with the given number of bits.
// fn random_prime_bits(bits: u32) -> ZZ {
//   if bits < 2 {
//     return ZZ::default();
//   }
//   let rng = &mut RNG.lock().unwrap();

//   loop {
//     let mut p = ZZ::from(ZZ::random_bits(bits-1, rng.deref_mut()));
//     p.set_bit(0, true);
//     p.toggle_bit(bits-1);
//     if p.is_probably_prime(30) != IsPrime::No {
//       return p;
//     }
//   }
// }



pub fn factor_rho(n: &Integer, upto: usize) -> Option<Integer> {
  let mut x = Integer::from(2);
  let mut cyc_len = 2;
  while cyc_len < upto {
    let y = x.clone();
    for _ in 0..cyc_len {
      x = (x.square() + 1) % n;
      let g = n.gcd_ref(&(&x - &y).into()).into();
      if g > 1 {
        return Some(g);
      }
    }
    cyc_len <<= 1;
  }
  None
}

pub fn factor_fermat(n: &Integer, range: impl RangeBounds<isize>) -> Option<Integer> {
  let (low, high) = discrete_bounds(range, 0u32, 10_000u32);
  let (mut a, mut b): (Integer, Integer) = n.sqrt_rem_ref().into();
  if b.cmp0().is_eq() {
    return Some(a);
  }

  b = -b;

  // n == a*a - b
  // n == (a+x)*(a+x) - (b + 2*a*x + x*x)

  let x = 1 + low as u64; // +1 because we're rounding up.
  b += &a * (2 * x);
  b += x * x;
  a += x;

  assert!(b.cmp0().is_ge());

  while low <= high {
    if b.is_perfect_square() {
      return Some(a - b.sqrt());
    }

    // n = a*a - b; n = (a+1)*(a+1) - (b+2*a+1)
    b += &a;
    a += 1;
    b += &a; // b += a+1
  }
  None
}

pub fn factor_square_forms(_n: &Integer, _k: u64, _steps: usize) -> Option<Integer> {
  todo!()
}
