use super::*;

/// # Generic univariate polynomials over a ring.
///
#[repr(transparent)]
#[derive(Clone,PartialEq,Debug)]
pub struct Polynomial<A>(A);

#[repr(transparent)]
#[derive(Clone,PartialEq,Debug,Hash)]
pub struct PolyElem<E>(Vec<E>);



// impl<A: AlgebraDisplay> AlgebraDisplay for Polynomial<A> {
//   fn fmt(&self, el: &Self::Element, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//     for (e,v) in el.0.iter().rev().enumerate() {
//       if self.0.is_zero(v) {
//         continue
//       } else if self.0.is_one(v) {

//       } else {
//         self.0.fmt(v, f)?;
//         write!()
//       }
//     }
//     Ok(())
//   }
// }

impl<A: Algebra> Algebra for Polynomial<A> {
  type Element = PolyElem<A::Element>;
}

impl<A: AdditiveMonoid> Polynomial<A> {
  pub fn new(domain: impl Into<A>) -> Self {
    Polynomial(domain.into())
  }

  pub fn elem(&self, els: Vec<A::Element>) -> PolyElem<A::Element> {
    let mut r = PolyElem(els);
    self.normalize(&mut r);
    r
  }

  #[inline]
  pub(crate) fn normalize(&self, a: &mut PolyElem<A::Element>) {
    let mut i = a.0.len();
    while i > 0 && self.0.is_zero(&a.0[i-1]) {
      i -= 1;
    }
    a.0.truncate(i);
  }
}

impl<E> PolyElem<E> {
  #[inline]
  pub fn deg(&self) -> isize {
    self.0.len() as isize - 1
  }

  #[inline]
  pub fn lc(&self) -> Option<&E> {
    self.0.last()
  }

  pub fn pop_lc(self) -> Option<(E, Self)> {
    let mut r = self;
    let lc = r.0.pop();
    lc.map(|c| (c,r))
  }
}

impl<E> Default for PolyElem<E> {
  #[inline]
  fn default() -> Self {
    PolyElem(Vec::new())
  }
}


impl<E> Rep for PolyElem<E> {
  type RepType = Vec<E>;
  fn rep(&self) -> &Self::RepType {
    &self.0
  }
}

// Make polynomials an additive monoid.

// impl<A: AdditiveMonoid + AlgebraFromInteger> AlgebraFromInteger for Polynomial<A> {
//   fn from_integer(&self, el: ZZ) -> Self::Element {
//     if el.cmp0().is_eq() {
//       self.zero()
//     } else {
//       self.from(vec![self.0.from_integer(el)])
//     }
//   }
// }

// impl<A: AdditiveMonoid> AlgebraFrom<Vec<A::Element>> for Polynomial<A> {
//   fn from<U: Into<Vec<A::Element>>>(&self, els: U) -> Self::Element {
//     let mut r = PolyElem(els.into());
//     self.normalize(&mut r);
//     r
//   }
// }

impl<A: AdditiveMonoid> AdditiveMonoid for Polynomial<A> {
  fn zero(&self) -> Self::Element {
    PolyElem(Vec::new())
  }
  fn is_zero(&self, a: &Self::Element) -> bool {
    a.0.len() == 0
  }
  fn add_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    let new_size = a.0.len().max(b.0.len());
    a.0.resize(new_size, self.0.zero());
    for (i,v) in b.0.iter().enumerate() {
      self.0.add_mut(&mut a.0[i], v)?;
    }
    self.normalize(a);
    Some(())
  }
  fn double_mut(&self, a: &mut Self::Element) -> Option<()> {
    for v in a.0.iter_mut() {
      self.0.double_mut(v)?;
    }
    self.normalize(a);
    Some(())
  }
}

// Make polynomials an additive group.

impl<A: AdditiveGroup> AdditiveGroup for Polynomial<A> {
  fn neg_mut(&self, a: &mut Self::Element) -> Option<()> {
    for v in a.0.iter_mut() {
      self.0.neg_mut(v)?;
    }
    Some(())
  }
  fn sub_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    let new_size = a.0.len().max(b.0.len());
    a.0.resize(new_size, self.0.zero());
    for (i,v) in b.0.iter().enumerate() {
      self.0.sub_mut(&mut a.0[i], v)?;
    }
    self.normalize(a);
    Some(())
  }
}

// Make polynomials a multiplicative monoid (if underlying algebra is a ring).
impl<A: Ring> MultiplicativeMonoid for Polynomial<A> {
  fn one(&self) -> Self::Element {
    PolyElem(vec![self.0.one()])
  }

  fn is_one(&self, a: &Self::Element) -> bool {
    a.0.len() == 1 && self.0.is_one(&a.0[0])
  }

  fn mul_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    if a.0.is_empty() || b.0.is_empty() {
      a.0.clear();
      return Some(())
    }
    let mut r = Vec::new();
    r.resize(a.0.len() + b.0.len() - 1, self.0.zero());
    for (i,x) in a.0.iter().enumerate() {
      for (j,y) in b.0.iter().enumerate() {
        self.0.muladd_mut(&mut r[i+j], &x, &y)?;
      }
    }
    a.0 = r;
    self.normalize(a);
    Some(())
  }
  fn square_mut(&self, a: &mut Self::Element) -> Option<()> {
    if a.0.is_empty() {
      return Some(())
    }
    let mut r = Vec::new();
    r.resize(2 * a.0.len() - 1, self.0.zero());
    for (i,x) in a.0.iter().enumerate() {
      for (j,y) in a.0.iter().enumerate() {
        self.0.muladd_mut(&mut r[i+j], &x, &y)?;
      }
    }
    a.0 = r;
    self.normalize(a);
    Some(())
  }
}

// Make polynomials a ring so long as the underlying domain is a ring.
impl<A: Ring> Ring for Polynomial<A> {
  fn from_integer(&self, a: ZZ) -> Self::Element {
    if a.cmp0().is_eq() {
      PolyElem::default()
    } else {
      PolyElem(vec![self.0.from_integer(a)])
    }
  }
}

// Make polynomials a pseudo-Euclidean/GCD domain.
//
// Basically just uses long division for now.
impl<A: EuclideanDomain> EuclideanDomain for Polynomial<A> {
  fn div(&self, a: Self::Element, b: &Self::Element) -> Option<Self::Element> {
    self.divrem(a, b.clone()).map(|(q,_)| q)
  }
  fn div_rhs(&self, a: Self::Element, b: &Self::Element) -> Option<Self::Element> {
    self.divrem_rhs(a, b.clone()).map(|(q,_)| q)
  }

  fn rem(&self, a: Self::Element, b: &Self::Element) -> Option<Self::Element> {
    self.divrem(a, b.clone()).map(|(_,r)| r)
  }
  fn rem_rhs(&self, a: Self::Element, b: &Self::Element) -> Option<Self::Element> {
    self.divrem_rhs(a, b.clone()).map(|(_,r)| r)
  }

  fn divrem(&self, a: Self::Element, b: Self::Element) -> Option<(Self::Element, Self::Element)> {
    let d = b.deg();
    if d > a.deg() {
      return Some((self.zero(), a));
    }
    let mut q = Vec::new();
    let mut r = a;
    q.resize((r.deg() - d + 1) as usize, self.0.zero());

    let (b_lc, b_rest) = b.pop_lc()?;
    while r.deg() >= d {
      let (s, check) = self.0.divrem(r.lc()?.clone(), b_lc.clone())?;
      if !self.0.is_zero(&check) {
        return None;
      }

      let i = r.deg() - d;
      let _ = r.0.pop();
      for (j,v) in b_rest.0.iter().enumerate() {
        self.0.mulsub_mut(&mut r.0[i as usize + j], &s, &v)?;
      }
      self.normalize(&mut r);

      q[i as usize] = s;
    }

    Some((PolyElem(q),r))
  }
}

impl<A: Ring> MonoidTimes for Polynomial<A> {
  fn times(&self, a: Self::Element, b: ZZ) -> Option<Self::Element> {
    a.0.into_iter()
       .map(|x| self.0.times(x, b.clone()))
       .collect::<Option<Vec<A::Element>>>()
      .map(PolyElem)
  }
}

impl<A: Ring> MonoidPower for Polynomial<A> {
  fn power(&self, a: Self::Element, b: ZZ) -> Option<Self::Element> {
    assert!(*b.as_abs() < 200_000);
    Generic::power_pos(self, a, b)
  }
}
