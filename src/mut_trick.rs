//! Partially stolen from crate `replace_with` but adapted for my circumstance.
//!

// use replace_with::on_unwind;
use std::{mem, ptr};

struct OnDrop<F: FnOnce()>(mem::ManuallyDrop<F>);
impl<F: FnOnce()> Drop for OnDrop<F> {
  #[inline(always)]
  fn drop(&mut self) {
    (unsafe { ptr::read(&*self.0) })();
  }
}

#[doc(hidden)]
#[inline(always)]
pub fn on_unwind<F: FnOnce() -> Option<T>, T, P: FnOnce()>(f: F, p: P) -> Option<T> {
  let x = OnDrop(mem::ManuallyDrop::new(p));
  match f() {
    Some(t) => {
      let mut x = mem::ManuallyDrop::new(x);
      unsafe { mem::ManuallyDrop::drop(&mut x.0) };
      Some(t)
    },
    None => None,
  }
}

// #[inline]
// pub fn replace_mut_<T,F,D>(dest: &mut T, f: F, d: D) -> Option<()>
// where
//   F: FnOnce(T) -> Option<T>,
//   D: FnOnce() -> T,
// {
//   *dest = f(*dest)
//   unsafe {
//     let old = ptr::read(dest);
//     on_unwind(move || f(old), || ptr::write(dest, Default::default())).map(move |new| ptr::write(dest, new))
//   }
// }


#[inline]
pub fn replace_mut<T,F>(dest: &mut T, f: F) -> Option<()>
where
  T: Default,
  F: FnOnce(T) -> Option<T>
{
  unsafe {
    let old = ptr::read(dest);
    on_unwind(move || f(old), || ptr::write(dest, Default::default())).map(move |new| ptr::write(dest, new))
  }
}

#[inline]
pub fn replace_mut2<T,F>(a: &mut T, b: &mut T, f: F) -> Option<()>
where
  T: Default,
  F: FnOnce(T,T) -> Option<(T,T)>
{
  unsafe {
    let olda = ptr::read(a);
    let oldb = ptr::read(b);
    on_unwind(
      move || f(olda, oldb),
      || {
        ptr::write(a, Default::default());
        ptr::write(b, Default::default());
      }).map(move |(newa,newb)| {
        ptr::write(b, newb);
        ptr::write(a, newa);
      })
  }
}
