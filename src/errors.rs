use std::{error::Error, fmt::{self, Display, Debug}};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum RntError<E> {
  Unknown,
  #[allow(dead_code)]
  NonUnit(E),
}

impl<E> Default for RntError<E> {
  fn default() -> Self { Self::Unknown }
}

impl<E: Display> Display for RntError<E> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self {
      RntError::Unknown => write!(f, "unknown error"),
      RntError::NonUnit(e) => write!(f, "non-unit: {}", e),
    }
  }
}

impl<E: Debug + Display> Error for RntError<E> { }

#[allow(dead_code)]
pub type RntResult<T,E> = Result<T,RntError<E>>;
