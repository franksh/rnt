//! Generic algorithms.
//!
//! These are intended for default implementations and they're probably very
//! slow.

use super::*;

pub struct Generic {

}

impl Generic {
  /// Positive integer power by square-and-mul algorithm.
  pub fn power_pos<A: MultiplicativeMonoid>(alg: &A, a: A::Element, b: ZZ) -> Option<A::Element> {
    match b.cmp0() {
      Ordering::Less => None,
      Ordering::Equal => Some(alg.one()),
      Ordering::Greater => {
        if b.significant_bits() == 1 {
          Some(a)
        } else if b.is_even() {
          Generic::power_pos(alg, alg.square(a)?, b >> 1)
        } else {
          alg.mul(a.clone(), &Generic::power_pos(alg, a, b >> 1)?)
        }
      },
    }
  }

  /// Generic square-and-mul power that works for negative exponents.
  pub fn power_zz<A: MultiplicativeGroup>(alg: &A, a: A::Element, b: ZZ) -> Option<A::Element> {
    match b.cmp0() {
      Ordering::Equal => Some(alg.one()),
      Ordering::Less => Generic::power_pos(alg, alg.inv(a)?, b.abs()),
      Ordering::Greater => Generic::power_pos(alg, a, b.abs()),
    }
  }

  /// Positive integer times by double-and-add algorithm.
  pub fn times_pos<A: AdditiveMonoid>(alg: &A, a: A::Element, b: ZZ) -> Option<A::Element> {
    match b.cmp0() {
      Ordering::Less => None,
      Ordering::Equal => Some(alg.zero()),
      Ordering::Greater => {
        if b.significant_bits() == 1 {
          Some(a)
        } else if b.is_even() {
          Generic::times_pos(alg, alg.double(a)?, b >> 1)
        } else {
          alg.add(a.clone(), &Generic::times_pos(alg, a, b >> 1)?)
        }
      },
    }
  }

  /// Generic square-and-mul power that works for negative exponents.
  pub fn times_zz<A: AdditiveGroup>(alg: &A, a: A::Element, b: ZZ) -> Option<A::Element> {
    match b.cmp0() {
      Ordering::Equal => Some(alg.zero()),
      Ordering::Less => Generic::times_pos(alg, alg.neg(a)?, b.abs()),
      Ordering::Greater => Generic::times_pos(alg, a, b.abs()),
    }
  }
}
