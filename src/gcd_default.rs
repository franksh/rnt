use super::*;


/// Default implementation for GCDDomain using Euclidean algorithm.
impl<A: EuclideanDomain> GCDDomain for A {
  default fn gcdext(&self, a: Self::Element, b: Self::Element) -> Option<(Self::Element,Self::Element,Self::Element)> {
    if self.is_zero(&b) {
      return Some((a, self.one(), self.zero()));
    }

    let (mut s0, mut s1) = (self.one(), self.zero());
    let (mut r0, mut r1) = (a.clone(), b.clone());

    while !self.is_zero(&r1) {
      let old_r = r1.clone();
      self.divrem_mut(&mut r0, &mut r1)?;
      // r0 is quotient, r1 is new remainder.

      // old_s, s = s, old_s - q*s
      self.mulsub_mut(&mut s0, &r0, &s1)?;
      std::mem::swap(&mut s0, &mut s1);

      r0 = old_r;
    }

    // t = r1 - q * a
    // TODO: this is pretty bad.
    let mut t = a;
    self.mul_mut(&mut t, &s0)?;
    self.neg_mut(&mut t)?;
    self.add_mut(&mut t, &r0)?;
    self.div_mut(&mut t, &b)?;

    Some((r0, s0, t))
  }

  default fn gcd(&self, a: Self::Element, b: Self::Element) -> Option<Self::Element> {
    let mut g = a;
    let mut r = b;
    while !self.is_zero(&r) {
      std::mem::swap(&mut g, &mut r);
      self.rem_mut(&mut r, &g)?;
    }
    Some(g)
  }

  default fn lcm(&self, a: Self::Element, b: Self::Element) -> Option<Self::Element> {
    let m = self.mul(a.clone(), &b)?;
    let g = self.gcd(a, b)?;
    self.div(m, &g)
  }
}


#[cfg(test)]
mod tests {
  use super::*;
  use std::str::FromStr;

  #[test]
  fn egcd_zero() {
    let alg = ZRing {};
    let a = alg.from_integer(ZZ::from(3*13*17));

    let (g,x,y) = alg.gcdext(a.clone(), alg.zero()).unwrap();
    assert_eq!( (&g,x,y), (&a, alg.one(), alg.zero()) );

    let (g,x,y) = alg.gcdext(alg.zero(), a.clone()).unwrap();
    assert_eq!( (&g,x,y), (&a, alg.zero(), alg.one()) );
  }

  #[test]
  fn egcd_1() {
    let alg = ZRing {};

    let a = alg.from_integer(ZZ::from_str("100000000000001").unwrap() * 17);
    let b = alg.from_integer(ZZ::from_str("100000000000003").unwrap() * 17);
    let (g,x,y) = alg.gcdext(a.clone(), b.clone()).unwrap();

    assert_eq!(g, ZZ::from(17));
    assert_eq!(x, ZZ::from(50000000000001i64));
    assert_eq!(y, ZZ::from(-50000000000000i64));
    assert_eq!(g, x*a + y*b);
  }

  #[test]
  fn egcd_2() {
    let alg = ZRing {};

    let common = ZZ::from(17 * 19 * 3 * 3 * 2 * 2 * 2);
    let a = alg.from_integer(ZZ::from_str("100000000000001").unwrap()) * &common;
    let b = alg.from_integer(ZZ::from_str("100000000000003").unwrap()) * &common;
    let (g,x,y) = alg.gcdext(a.clone(), b.clone()).unwrap();

    assert_eq!(g, common);
    assert_eq!(x, ZZ::from(50000000000001i64));
    assert_eq!(y, ZZ::from(-50000000000000i64));
    assert_eq!(g, x*a + y*b);
  }

  #[test]
  fn egcd_random() {
    let alg = ZRing {};
    for (a,b,g,x,y) in [(31306453669459010i128, 175485402824395328i128, 14i128, 5439141650160711i128, -970338463095857i128),
                        (2253847721002924200i128, 14582345975830766496i128, 5016i128, -303838037111577i128, 46961213828896i128),
                        (18441966528381000i128, 10722889571173000000i128, 17000i128, 289067040185357i128, -497157472729i128),
                        (4682675902816831920i128, 5219153138227608000i128, 240i128, 5089617425351197i128, -4566455177887776i128),
                        (5280561923879264256i128, 5622256522878464i128, 512i128, -5291474361980i128, 4969883164055503i128),
                        (5001499997875920000i128, 2819581870709306647i128, 7i128, 57443543611420769i128, -101895918056187119i128),
                        (2802950882703798048i128, 4501448302742412736i128, 1376i128, -463678620850967i128, 288722276075747i128),
                        (154710302629064608i128, 959822406274108000i128, 32i128, 13480173414156954i128, -2172820403820523i128),
                        (1541261733224259735i128, 294758243916047872i128, 1i128, 107317782249227559i128, -561154079620376087i128),
                        (83138047108503750i128, 1807863390113863743i128, 9i128, 70196280492039667i128, -3228109881699287i128)] {
      let a = alg.from_integer(ZZ::from(a));
      let b = alg.from_integer(ZZ::from(b));
      let (hg,hx,hy) = alg.gcdext(a.clone(), b.clone()).unwrap();
      assert_eq!(ZZ::from(g), hg);
      assert_eq!(ZZ::from(x), hx);
      assert_eq!(ZZ::from(y), hy);
    }
  }
}
