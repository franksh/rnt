use std::ops::ShlAssign;

use rug::{Assign, ops::RemRoundingAssign};

use super::*;

/// # `mod n` Modular arithmetic.
///
/// The modulus must be positive but is not required to be prime so this is
/// merely a ring, not necessarily a field.
///
/// Elements are _always_ within the range `[0,n)`.
///
/// It is an error to use a `ZModElem` from a "foreign" `ZMod`, doing so might
/// result in a crash or panic.
#[repr(transparent)]
#[derive(Clone,PartialEq)]
pub struct ZMod(ZZ);

#[repr(transparent)]
#[derive(Clone,PartialEq,Default,Hash,Debug)]
pub struct ZModElem(ZZ);


impl ZMod {
  pub fn new(n: impl Into<ZZ>) -> Option<ZMod> {
    let n = n.into();
    if n < 1 { None } else { Some(Self(n)) }
  }

  pub fn neg_rep(&self, el: &ZModElem) -> ZZ {
    let mut m = self.0.clone();
    m >>= 1;
    if el.0 > m {
      m.assign(&el.0 - &self.0);
    } else {
      m.assign(&el.0)
    }
    m
  }
}


impl Algebra for ZMod {
  type Element = ZModElem;
}

impl Rep for ZMod {
  type RepType = ZZ;
  #[inline]
  fn rep(&self) -> &Self::RepType {
    &self.0
  }
}

impl Rep for ZModElem {
  type RepType = ZZ;
  #[inline]
  fn rep(&self) -> &Self::RepType {
    &self.0
  }
}

// impl AlgebraFromInteger for ZMod {
//   fn from_integer(&self, a: ZZ) -> Self::Element {
//     if a.cmp0().is_ge() && a < self.0 {
//       ZModElem(a)
//     } else {
//       ZModElem(a.rem_euc(&self.0))
//     }
//   }
// }

impl AdditiveMonoid for ZMod {
  fn zero(&self) -> Self::Element {
    ZModElem(ZZ::default())
  }
  fn is_zero(&self, a: &ZModElem) -> bool {
    a.0.cmp0().is_eq()
  }
  fn add_mut(&self, a: &mut ZModElem, b: &ZModElem) -> Option<()> {
    a.0 += &b.0;
    if a.0 >= self.0 {
      a.0 -= &self.0;
    }
    Some(())
  }
  fn double_mut(&self, a: &mut ZModElem) -> Option<()> {
    a.0.shl_assign(1);
    if a.0 >= self.0 {
      a.0 -= &self.0;
    }
    Some(())
  }
}

impl AdditiveGroup for ZMod {
  fn neg_mut(&self, a: &mut ZModElem) -> Option<()> {
    a.0.sub_from(&self.0);
    Some(())
  }

  fn sub_mut(&self, a: &mut ZModElem, b: &ZModElem) -> Option<()> {
    a.0 -= &b.0;
    if a.0.cmp0().is_lt() {
      a.0 += &self.0;
    }
    Some(())
  }
}

impl MultiplicativeMonoid for ZMod {
  // TODO: Modulus might be 1.
  fn one(&self) -> ZModElem {
    ZModElem(ZZ::from(1))
  }
  // TODO: Modulus might be 1.
  fn is_one(&self, a: &ZModElem) -> bool {
    a.0 == 1
  }

  fn mul_mut(&self, a: &mut ZModElem, b: &ZModElem) -> Option<()> {
    a.0 *= &b.0;
    a.0.rem_euc_assign(&self.0);
    Some(())
  }
  fn square_mut(&self, a: &mut ZModElem) -> Option<()> {
    a.0.square_mut();
    a.0.rem_euc_assign(&self.0);
    Some(())
  }
}

impl MultiplicativeGroup for ZMod {
  fn inv_mut(&self, a: &mut ZModElem) -> Option<()> {
    a.0.invert_mut(&self.0).ok()
  }

  fn div_mut(&self, a: &mut ZModElem, b: &ZModElem) -> Option<()> {
    a.0 *= b.clone().0.invert(&self.0).ok()?;
    a.0 %= &self.0;
    Some(())
  }

  fn div_rhs_mut(&self, a: &mut ZModElem, b: &ZModElem) -> Option<()> {
    self.div_mut(a, b) // Commutative
  }
}

impl Ring for ZMod {
  fn from_integer(&self, n: ZZ) -> ZModElem {
    ZModElem(n.rem_euc(&self.0))
  }
}

impl MonoidTimes for ZMod {
  fn times(&self, a: ZModElem, b: ZZ) -> Option<ZModElem> {
    Some(ZModElem((a.0 * b).rem_euc(&self.0)))
  }
}

impl MonoidPower for ZMod {
  fn power(&self, a: ZModElem, b: ZZ) -> Option<ZModElem> {
    a.0.pow_mod(&b, &self.0).ok().map(|z| ZModElem(z))
  }
}
