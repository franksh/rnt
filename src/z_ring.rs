use std::ops::{AddAssign, ShlAssign, ShrAssign, SubAssign};

use rug::ops::{DivRoundingAssign, NegAssign, Pow, RemRoundingAssign};
use super::*;

/// "Empty" default algebra implemented for Integers.
///
#[derive(Clone,PartialEq)]
pub struct ZRing;

impl Algebra for ZRing {
  type Element = ZZ;
}


// macro_rules! zring_impl_alg {
//   ($trait:ident ($lhs:ident) { $($code:tt)+ }) => {
//     paste! {
//       impl [<Algebra $trait:camel>] for ZRing {
//         #[inline]
//         fn [<$trait:snake>](&self, $lhs: Self::Element) -> Self::Element {
//           $($code)+
//         }
//       }
//     }
//   };
//   (mut $trait:ident ($lhs:ident , $rhs:ident) { $($code:tt)+ }) => {
//     paste! {
//       impl [<Algebra $trait:camel Mut>] for ZRing {
//         #[inline]
//         fn [<$trait:snake _mut>](&self, $lhs: &mut Self::Element, $rhs: &Self::Element) -> bool {
//           $($code)+
//         }
//       }
//     }
//   };
//   ($trait:ident ($lhs:ident , $rhs:ident) { $($code:tt)+ }) => {
//     zring_impl_alg!($trait($lhs : Self::Element, $rhs : &Self::Element) { $($code)+ });
//   };
//   ($trait:ident ($lhs:ident : $lhsT:ty , $rhs:ident : $rhsT:ty) { $($code:tt)+ }) => {
//     zring_impl_alg!($trait($lhs : $lhsT, $rhs : $rhsT) -> Self::Element { $($code)+ });
//   };
//   ($trait:ident ($lhs:ident : $lhsT:ty , $rhs:ident : $rhsT:ty) -> $retT:ty { $($code:tt)+ }) => {
//     paste! {
//       impl [<Algebra $trait:camel>] for ZRing {
//         #[inline]
//         fn [<$trait:snake>](&self, $lhs: $lhsT, $rhs: $rhsT) -> $retT {
//           $($code)+
//         }
//       }
//     }
//   };
// }

// Make ZRing an additive monoid.

impl AdditiveMonoid for ZRing {
  #[inline]
  fn zero(&self) -> ZZ {
    ZZ::default()
  }
  #[inline]
  fn is_zero(&self, a: &ZZ) -> bool {
    a.cmp0().is_eq()
  }
  #[inline]
  fn add_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    *a += b;
    Some(())
  }
  #[inline]
  fn double_mut(&self, a: &mut ZZ) -> Option<()> {
    a.shl_assign(1);
    Some(())
  }
}

// Make ZRing an additive group.

impl AdditiveGroup for ZRing {
  #[inline]
  fn neg_mut(&self, a: &mut ZZ) -> Option<()> {
    a.neg_assign();
    Some(())
  }
  #[inline]
  fn sub_mut(&self, a: &mut ZZ, b: &ZZ) -> Option<()> {
    *a -= b;
    Some(())
  }
}

// Make ZRing a multiplicative monoid.

impl MultiplicativeMonoid for ZRing {
  #[inline]
  fn one(&self) -> ZZ {
    ZZ::from(1)
  }
  #[inline]
  fn is_one(&self, a: &ZZ) -> bool {
    a == &1
  }
  #[inline]
  fn mul_mut(&self, a: &mut Self::Element, b: &Self::Element) -> Option<()> {
    *a *= b;
    Some(())
  }
  #[inline]
  fn square_mut(&self, a: &mut ZZ) -> Option<()> {
    a.square_mut();
    Some(())
  }
}


// Make ZRing a Euclidean domain.

impl EuclideanDomain for ZRing {
  #[inline]
  fn divrem_mut(&self, a: &mut ZZ, b: &mut ZZ) -> Option<()> {
    if b.cmp0().is_eq() {
      None
    } else {
      a.div_rem_euc_mut(b);
      Some(())
    }
  }
  #[inline]
  fn rem_mut(&self, a: &mut ZZ, b: &ZZ) -> Option<()> {
    if b.cmp0().is_eq() {
      None
    } else {
      a.rem_euc_assign(b);
      Some(())
    }
  }
  #[inline]
  fn div_mut(&self, a: &mut ZZ, b: &ZZ) -> Option<()> {
    if b.cmp0().is_eq() {
      None
    } else {
      a.div_euc_assign(b);
      Some(())
    }
  }
}

impl GCDDomain for ZRing {
  fn gcdext(&self, a: ZZ, b: ZZ) -> Option<(ZZ, ZZ, ZZ)> {
    Some(a.gcd_cofactors(b, ZZ::new()))
  }

  fn gcd(&self, a: ZZ, b: ZZ) -> Option<ZZ> {
    Some(a.gcd(&b))
  }

  fn lcm(&self, a: ZZ, b: ZZ) -> Option<ZZ> {
    Some(a.lcm(&b))
  }
}

impl Ring for ZRing {
  #[inline]
  fn from_integer(&self, n: ZZ) -> ZZ {
    n
  }

  #[inline]
  fn muladd_mut(&self, a: &mut ZZ, b: &ZZ, c: &ZZ) -> Option<()> {
    a.add_assign(b * c);
    Some(())
  }

  #[inline]
  fn mulsub_mut(&self, a: &mut ZZ, b: &ZZ, c: &ZZ) -> Option<()> {
    a.sub_assign(b * c);
    Some(())
  }
}

impl MonoidTimes for ZRing {
  fn times(&self, a: ZZ, b: ZZ) -> Option<ZZ> {
    Some(a*b)
  }
}

impl MonoidPower for ZRing {
  fn power(&self, a: ZZ, b: ZZ) -> Option<ZZ> {
    b.to_u32().map(|n| a.pow(n))
  }
}
